#!/usr/bin/env perl

=for Licensee
#####BSD License Definition#####
#    Copyright 2013 Windstream. All rights reserved.
#    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#    THIS SOFTWARE IS PROVIDED BY Windstream "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL WINDSTREAM OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#    The views and conclusions contained in the software and documentation are those of the authors and should not be interpreted as representing official policies, either expressed or implied, of Windstream.
################################
=cut

=for Metadata
#Created:8/26/2013
#Author:Michael Bostwick

#Updated: 9/10/2013
#Author:Michael Bostwick
#update reason:Fixed Label issue, encoding issue, fixed error that swapped the constants for $VersionExFolder and $ComponentsExFolder in the final export, removed output folder on startup

#Declared Version:2.00
#Purpose:The Purpose of the script is to move the xml jira backup export to data that can be imported into a different JIRA instance.
=cut

=for Assumptions
#<!--ISSUE types must match in the remote system -->
#<!--see comments on the hash for seeing assumings of xml on global items -->
#<!--jira schema definition assumed -->
#second go around finds these and writes them as they find them
#<project>
#
#<Component>
#id, project, name, description
#lead
#</Component>
#
#<Version>
#id, project, name, description, sequence, released, startdate, releasedate, archived
#</Version>
#
#<ProjectRoleActor>
#id, pid, projectroleid, roletype
#roletypeparameter
#<!--#roletypeparameter="chadwight"-->
#</ProjectRoleActor>
#
#<!--###############-->
#<issue>
#id, key, project, type, summary, description, priority, resolution, status, created, duedate, date, timeestimate, workflow, environment
#reporter,assignee
#
#<FileAttachment><!--attachments-->
#id, issue, filename, created
#author
#</FileAttachment>
#
#<Action type="comment"> <!--comments-->
#id, issue,type, body, created
#author
#</Action>
#
#<Worklog>
#id, issue, body, created, startdate, timeworked
#author
#</Worklog>
#
#<Label>
#issue, label
#</Label>
#</issue>
#
#<!--###############-->
#<links>
#<IssueLink>
#id, linktype, source, destination
#</IssueLink>
#
#<RemoteIssueLink>
#id, issueid, title, url, relationship, applicationtype, applicationname
#</RemoteIssueLink>
#</links>
#<!--###############-->
#
#</project>

=cut

=for Possible_Improvement
#1.all projects are put into one hash. For alot of projects, that could use alot of memeory. (the same could be said for all global items)
##1A-Reason Not Included:It would take more time.Also, memory to disk trade off..
#2.Dosn't support CustomField or CustomFieldValue (also fields that use CDATA are looked up manually)
#3.Fields that use CDATA are set manually, they should be dynamic especially if #2 is considered  
#4.all of link data is loaded into memory, so on a system with lots of links there may be issues
#5.Memory could be improved allot on this via diffrent hashing and work arounds..
#6.Subs could be orgnised or broken up better...
#7.there are several todo's that could be finshed
#8.All things that can't be imported could be mapped to jelly or the api
=cut

use XML::Parser;
use JIRAImportGlobalObjects;
use JIRAImportObjects;
use JSON-convert_blessed_universally;
use File::Copy;
use File::Path 'rmtree';
use POSIX qw/strftime/;
#use feature 'unicode_strings';

#use open ':encoding(utf8)';

use warnings;
use strict;

use Readonly;

Readonly my $DebugMode        => "N";
Readonly my $path2UserNameMap => "userMap.txt";
Readonly my $path2XMLImport   => "entities.xml";
Readonly my $fileName4FullExport   => "all_projects";
Readonly my $path2Export      => "./Output/";
Readonly my $slash4Folder     => "/";

Readonly my $issuesExFolder     => "Issues";
Readonly my $linksExFolder      => "Links";
Readonly my $ComponentsExFolder => "Components";
Readonly my $VersionExFolder    => "Versions";
Readonly my $unmappedExFolder   => "no_folder";

Readonly my $attachmentExFolder => "FileAttachment";
Readonly my $commentExFolder    => "comment";
Readonly my $worklogExFolder    => "Worklog";



Readonly my $labelsExSuffix    => "_labels";
Readonly my $sep4Labels        => "\n";
Readonly my $exportFilesSuffix => ".txt";
Readonly my $systemImportLabel => "PJImport".strftime("%Y%m%d", localtime);

Readonly my $subTaskLinkType => "jira_subtask_link";

#Readonly my $attachmentURL =>
#  '';
#Readonly my $avatarUrl =>
#  '';
#my %projectKey2WorkFlowMap = ( );

use ExampleCompany qw( $JiraURL );

Readonly my $attachmentURL =>  $JiraURL."attachment/";
Readonly my $avatarUrl =>  $JiraURL."projectavatar?";

#any projects that need to be mapped should be mapped here
my %projectKey2WorkFlowMap = ( 'OSPD' => 'Copy of classic' );
  
#Readonly my $defaultWorkflow =>'Classic Workflow Scheme';
Readonly my $defaultWorkflow => undef;

rmtree $path2Export;
mkdir $path2Export;
#the XML object names are assumed to be listed as Below (substring global)
my %globalProject  = ();    #id, object{name,description,key,avatar}
my %globalPriority = ();    #id, object{sequence, name, description}
my %globalStatus   = ();    #id, object{sequence, name, description, iconurl}
my %globalResolution =
  ();    #id, object{sequence, name, description} #World PEACE ?
my %globalWorkFlow      = ();    #id, name
my %globalIssueLinkType = ();    #id, object{linkname, inward, outward, style}
my %globalIssueType =
  ();    #id, object {name,sequence, style, description, iconurl}
#####################

my %userNameMaps = fillUserNameMap( $path2UserNameMap, $DebugMode );

#
#
my $parser = XML::Parser->new( ErrorContext => 2 );
$parser->setHandlers( Start => \&start_FirstGlobalXmlHandler );
$parser->parsefile( $path2XMLImport, ProtocolEncoding => 'UTF-8' );
#
writeGlobalParamatersOut()
  ; #reads the global hashes and writes them to the needed format and creates project folders
makeGlobalFolders();
#
my %issueId2Project    = ();    #issue id to project id
my $currentOpenXmlNode = "";    #this is used for nodes that have more data
my $childTextXMLNode   = "";
my $pointer2ObjectWorkingOn;
my $project4PendingOp;
my %subTasks = ();              #parent id, array(subtask_ids)

#counters to store files with out ids
my $fileAtchCount = 0;
my $commentCount  = 0;
my $workLogCount  = 0;
#local $SIG{__WARN__} = sub {if( not ($_[0]  =~ /[^Wide character in print]/)){print $_[0];}}; #silencing all wide character warnings
#local $SIG{__WARN__} = \&handleErrorMsgs;
#local $SIG{__WARN__} = sub { die $_[0] }; #die on warnings

$parser = XML::Parser->new( ErrorContext => 2 );
$parser->setHandlers(
                      Start => \&start_SecondXmlHandler,
                      Char  => \&text_xmlHandlerSecondRound,
                      End   => \&end_SecondXmlHandler
);
$parser->parsefile( $path2XMLImport, ProtocolEncoding => 'UTF-8' );
saveLinkMap();

#loadLinkMap();
moveUnmappedFolders();
buildFinalOutPutProjectFiles();

sub handleErrorMsgs
{
   my ($errorMsg) = @_;
   if($DebugMode ne 'Y'){
      if( not ($errorMsg  =~ /[^Wide character in print]/))
      {
         print $errorMsg;
      }
   }else{
      print $errorMsg;
   }
}

sub fillUserNameMap
{
   my ( $path2UserNameMapping, $debugging ) = @_;
   my %mapOfUserNames = ();

   open my $fh, '<', $path2UserNameMapping or die "Unable to open file:$!\n";
   while (<$fh>)
   {
      if ( defined($_) )
      {
         chomp;
         my ( $key, $val ) = split /=/;
         $mapOfUserNames{$key} = $val;
      }
   }
   close $fh;
   if ( $debugging eq 'Y' )
   {
      print "$_ => $mapOfUserNames{$_}\n" for keys %mapOfUserNames;
   }

   return %mapOfUserNames;
}

sub start_FirstGlobalXmlHandler
{
   my ( $expat, $element, %attrval ) = @_;
   if ( $element eq 'Project' )
   {
      my $gPobject = JIRAImportGlobalObjects::Project->new;
      $gPobject->id( $attrval{"id"} );
      $gPobject->name( $attrval{"name"} );
      $gPobject->description($attrval{"description"} );
      #TODO:the description can be CDATA but it wont capture that right now
      $gPobject->key( $attrval{"key"} );
      $gPobject->avatar( $attrval{"avatar"} );
      $globalProject{ $attrval{"id"} } = $gPobject;
   }
   elsif ( $element eq 'Priority' )
   {
      my $gPobject = JIRAImportGlobalObjects::Priority->new;
      $gPobject->id( $attrval{"id"} );
      $gPobject->sequence( $attrval{"sequence"} );
      $gPobject->name( $attrval{"name"} );
      $gPobject->description( $attrval{"description"} );
      $globalPriority{ $attrval{"id"} } = $gPobject;
   }
   elsif ( $element eq 'Status' )
   {
      my $gSobject = JIRAImportGlobalObjects::Status->new;
      $gSobject->id( $attrval{"id"} );
      $gSobject->sequence( $attrval{"sequence"} );
      $gSobject->name( $attrval{"name"} );
      $gSobject->description( $attrval{"description"} );
      $gSobject->iconurl( $attrval{"iconurl"} );
      $globalStatus{ $attrval{"id"} } = $gSobject;
   }
   elsif ( $element eq 'Resolution' )
   {
      my $gRobject = JIRAImportGlobalObjects::Resolution->new;
      $gRobject->id( $attrval{"id"} );
      $gRobject->sequence( $attrval{"sequence"} );
      $gRobject->name( $attrval{"name"} );
      $gRobject->description( $attrval{"description"} );
      $globalResolution{ $attrval{"id"} } = $gRobject;
   }
   elsif ( $element eq 'Workflow' )
   {
      $globalWorkFlow{ $attrval{"id"} } = $attrval{"name"};
   }
   elsif ( $element eq 'IssueLinkType' )
   {
      my $gIobject = JIRAImportGlobalObjects::IssueLinkType->new;
      $gIobject->id( $attrval{"id"} );
      $gIobject->linkname( $attrval{"linkname"} );
      $gIobject->inward( $attrval{"inward"} );
      $gIobject->outward( $attrval{"outward"} );
      $gIobject->style( $attrval{"style"} );
      $globalIssueLinkType{ $attrval{"id"} } = $gIobject;
   }
   elsif ( $element eq 'IssueType' )
   {
      my $gIobject = JIRAImportGlobalObjects::IssueType->new;
      $gIobject->id( $attrval{"id"} );
      $gIobject->name( $attrval{"name"} );
      $gIobject->sequence( $attrval{"sequence"} );
      $gIobject->style( $attrval{"style"} );
      $gIobject->description( $attrval{"description"} );
      $gIobject->iconurl( $attrval{"iconurl"} );
      $globalIssueType{ $attrval{"id"} } = $gIobject;
   }
   return;
}

sub writeGlobalParamatersOut
{
   my $tmpFileHandle;
   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   open( $tmpFileHandle,
         '>' . $path2Export . 'Priorities' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalPriority );
   close($tmpFileHandle);

   open( $tmpFileHandle, '>' . $path2Export . 'Statuses' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalStatus );
   close($tmpFileHandle);

   open( $tmpFileHandle,
         '>' . $path2Export . 'Resolutions' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalResolution );
   close($tmpFileHandle);

   open( $tmpFileHandle,
         '>' . $path2Export . 'Workflows' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalWorkFlow );
   close($tmpFileHandle);

   open( $tmpFileHandle,
         '>' . $path2Export . 'IssueLinkTypes' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalIssueLinkType );
   close($tmpFileHandle);

   open( $tmpFileHandle,
         '>' . $path2Export . 'IssueTypes' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalIssueType );
   close($tmpFileHandle);

   open( $tmpFileHandle, '>' . $path2Export . 'Projects' . $exportFilesSuffix );
   print $tmpFileHandle $json_obj->encode( \%globalProject );
   close($tmpFileHandle);

   return;
}

sub makeGlobalFolders
{
   foreach my $key ( keys %globalProject )
   {
      my $key4Dir = $globalProject{$key}->key;
      mkdir $path2Export . $key4Dir;
      mkdir $path2Export . $key4Dir . $slash4Folder . $VersionExFolder;
      mkdir $path2Export . $key4Dir . $slash4Folder . $ComponentsExFolder;
      mkdir $path2Export . $key4Dir . $slash4Folder . $issuesExFolder;
      mkdir $path2Export
        . $key4Dir
        . $issuesExFolder
        . $slash4Folder
        . $unmappedExFolder;
   }

   mkdir $path2Export . $linksExFolder;
   mkdir $path2Export . $unmappedExFolder;
}

sub start_SecondXmlHandler
{
   my ( $expat, $element, %attrval ) = @_;
   my @array4ItemSet = ();
   if ( $element eq 'Component' )
   {
      my $componentObject = JIRAImportObjects::Component->new;
      $componentObject->name( $attrval{"name"} );
      $componentObject->description( $attrval{"description"} );
      $componentObject->lead( getUserName( $attrval{"lead"} ) );
      writeJIRAObjectOut( $attrval{"project"}, 'Component', $componentObject );
   }
   elsif ( $element eq 'Version' )
   {
      my $versionObject = JIRAImportObjects::Version->new;
      $versionObject->name( $attrval{"name"} );                  #'name'
      $versionObject->description( $attrval{"description"} );    #'description'
      $versionObject->archived( $attrval{"archived"} );          #'archived'
      $versionObject->released( $attrval{"released"} );          #'released'
      writeJIRAObjectOut( $attrval{"project"}, 'Version', $versionObject );
   }
   elsif ( $element eq 'ProjectRoleActor' )
   {
#TODO:Right now BOTH CSV and JIRA Objects don't support setting roles to projects, so just going to leave this empty (i.e the interface there both using dosn't support it)
#so this is just going to be here as a place holder(I may go ahead and export it anyways..)
   }
   elsif ( $element eq 'Issue' )
   {
      $currentOpenXmlNode = 'Issue';
      $project4PendingOp  = $attrval{"project"};
      my $issueObject = JIRAImportObjects::Issue->new;
      $pointer2ObjectWorkingOn = \$issueObject;

      $issueObject->externalId( $attrval{"id"} );    #$attrval{"key"} not saved
      $issueObject->summary( $attrval{"summary"} );

      $issueObject->originalEstimate( formatTimeInput($attrval{"timeoriginalestimate"} ));
      $issueObject->estimate( formatTimeInput($attrval{"timeestimate"} ));

      $issueObject->updated( formatDateTime($attrval{"updated"}));
      $issueObject->created( formatDateTime($attrval{"created"} ));
      $issueObject->duedate( formatDateTime($attrval{"duedate"} ));

      $issueObject->reporter( getUserName( $attrval{"reporter"} ) );
      $issueObject->assignee( getUserName( $attrval{"assignee"} ) );

      #needs id's mapped to names
      $issueObject->priority( getPriorityName( $attrval{"priority"} ) );
      $issueObject->resolution( getResolutionName( $attrval{"resolution"} ) );
      $issueObject->status( getStatusName( $attrval{"status"} ) );
      $issueObject->issueType( getIssueTypeName( $attrval{"type"} ) );

      #can have cdata
      $issueObject->description( $attrval{"description"} );
      $issueObject->environment( $attrval{"environment"} );

   }
   elsif ( $element eq $attachmentExFolder )
   {
      my $attachmentObject = JIRAImportObjects::Attachment->new;
      $attachmentObject->name( $attrval{"filename"} );
      $attachmentObject->created( formatDateTime($attrval{"created"} ));
      $attachmentObject->attacher( getUserName( $attrval{"author"} ) );
      $attachmentObject->uri( getAttachmentURI( $attrval{"id"} ) );
      writeJIRAObjectOut( $attrval{"issue"}, $attachmentExFolder,
                          $attachmentObject );
   }
   elsif ( $element eq 'Action' and $attrval{"type"} eq $commentExFolder )
   {
      $currentOpenXmlNode = $commentExFolder;
      $project4PendingOp  = $attrval{"issue"};
      my $commentObject = JIRAImportObjects::Comment->new;
      $pointer2ObjectWorkingOn = \$commentObject;

      $commentObject->author( getUserName( $attrval{"author"} ) );
      $commentObject->created(formatDateTime($attrval{"created"} ));
      $commentObject->body( $attrval{"body"} );    #can have cdata

   }
   elsif ( $element eq $worklogExFolder )
   {
      $currentOpenXmlNode = $worklogExFolder;
      $project4PendingOp  = $attrval{"issue"};
      my $worklogObject = JIRAImportObjects::WorkLog->new;
      $pointer2ObjectWorkingOn = \$worklogObject;

      $worklogObject->author( getUserName( $attrval{"author"} ) );
      $worklogObject->timeSpent( formatTimeInput($attrval{"timeworked"} ));
      $worklogObject->startDate( formatDateTime($attrval{"startDate"} ));
      $worklogObject->comment( $attrval{"body"} );    #can have cdata
   }
   elsif ( $element eq 'Label' )
   {
      writeJIRAObjectOut( $attrval{"issue"}, 'Label', $attrval{"label"} );
   }
   elsif ( $element eq 'IssueLink' )
   {
      my $link2Save = JIRAImportObjects::Link->new;
      $link2Save->name( getLinkNameFromId( $attrval{"linktype"} ) );
      $link2Save->sourceId( $attrval{"source"} );
      $link2Save->destinationId( $attrval{"destination"} );
      writeJiraLinkObjectOut( $attrval{"id"}, $link2Save );
   }
   elsif ( $element eq 'RemoteIssueLink' )
   {
#TODO: JIRAImportObjects::RemoteLink covers these but theres no way to import them so I'm just going to let them die (they have to be updated in remote system)
#id, issueid, title, url, relationship, applicationtype, applicationname
   }
   return;
}

sub text_xmlHandlerSecondRound
{
   my ( $expat, $text ) = @_;
   if (    $currentOpenXmlNode eq 'Issue'
        or $currentOpenXmlNode eq 'Worklog'
        or $currentOpenXmlNode eq 'comment' )
   {
      $childTextXMLNode = $text;
      if ( $childTextXMLNode eq '\n' )
      {
         print "Found new line data as result";
      }

 #$childTextXMLNode = s/<!\[CDATA\[([^]+]*)\]\]>/$1/;#removing the CDATA wrapper
 # one line up >Use of uninitialized value $_ in substitution (s///)
   }
   return;
}

sub end_SecondXmlHandler
{
   my ( $expat, $element, @attrval ) = @_;

   if ( $currentOpenXmlNode eq 'Issue' )
   {
      if ( $element eq 'description' )
      {
         ${$pointer2ObjectWorkingOn}->description($childTextXMLNode);
      }
      elsif ( $element eq 'environment' )
      {
         ${$pointer2ObjectWorkingOn}->environment($childTextXMLNode);
      }
      else
      {
         writeJIRAObjectOut( $project4PendingOp, $currentOpenXmlNode,
                             $$pointer2ObjectWorkingOn );
         $currentOpenXmlNode = "";
      }
   }
   elsif ( $currentOpenXmlNode eq $worklogExFolder )
   {
      if ( $element eq 'body' )
      {
         ${$pointer2ObjectWorkingOn}->comment($childTextXMLNode);
      }
      else
      {
         writeJIRAObjectOut( $project4PendingOp, $currentOpenXmlNode,
                             $$pointer2ObjectWorkingOn );
         $currentOpenXmlNode = "";
      }
   }
   elsif ( $currentOpenXmlNode eq $commentExFolder )
   {
      if ( $element eq 'body' )
      {
         ${$pointer2ObjectWorkingOn}->body($childTextXMLNode);
      }
      else
      {
         writeJIRAObjectOut( $project4PendingOp, $currentOpenXmlNode,
                             $$pointer2ObjectWorkingOn );
         $currentOpenXmlNode = "";
      }
   }
   else
   {
      $currentOpenXmlNode = "";
   }
   return;
}

sub formatDateTime
{
   my ($inputTime ) = @_;
   my $outputTime;
   if(defined($inputTime))
   {
      #Dates can be represented in SimpleDateFormat "yyyy-MM-dd'T'HH:mm:ss.SSSZ" (example output: "2012-08-31T15:59:02.161+0100") 
      #or you can use relative dates like "P-1D" (which means one day ago).
      $outputTime = $inputTime;
      $outputTime =~ s/ /T/g;
   }
   return $outputTime;
}

sub formatTimeInput
{
   my ($inputEst) = @_;
   my $outputEst = $inputEst;
   if(defined $outputEst){
      $outputEst = 'PT'.$inputEst.'S';
   }
   return $outputEst;
}

sub getPriorityName
{
   my ($id4Priority) = @_;
   my $priName2Return = undef;
   if ( defined $id4Priority )
   {
      $priName2Return = $globalPriority{$id4Priority}->name;
   }
   return $priName2Return;
}

sub getResolutionName
{
   my ($resolutionId) = @_;
   my $resName2Return = undef;
   if ( defined $resolutionId )
   {
      $resName2Return = $globalResolution{$resolutionId}->name;
   }
   return $resName2Return;
}

sub getStatusName
{
   my ($statusId) = @_;
   return $globalStatus{$statusId}->name;
}

sub getIssueTypeName
{
   my ($typeId) = @_;
   return $globalIssueType{$typeId}->name;
}

sub getAttachmentURI
{
   my ($atFileID) = @_;
   return $attachmentURL . $atFileID . '/';
}

sub getProjectNameFromId
{
   my ($projectId) = @_;
   my $key2Return = "";
   if ( defined $projectId )
   {
      $key2Return = $globalProject{$projectId}->key;
   }
   return $key2Return;
}

sub foundIssue2ProjectLink
{
   my ($issueId) = @_;
   my $val2Return = isThereAProjectLink($issueId);
   if ( !$val2Return )
   {
      mkdir $path2Export . $unmappedExFolder . $slash4Folder . $issueId;
      mkdir $path2Export
        . $unmappedExFolder
        . $slash4Folder
        . $issueId
        . $slash4Folder
        . $attachmentExFolder;
      mkdir $path2Export
        . $unmappedExFolder
        . $slash4Folder
        . $issueId
        . $slash4Folder
        . $commentExFolder;
      mkdir $path2Export
        . $unmappedExFolder
        . $slash4Folder
        . $issueId
        . $slash4Folder
        . $worklogExFolder;
   }

   return $val2Return;
}

sub isThereAProjectLink
{
   my ($issueId) = @_;

   return exists $issueId2Project{$issueId};
}

sub getProjectIdFromIssueId
{
   my ($issueId) = @_;
   if ( !isThereAProjectLink($issueId) )
   {
      if ( $DebugMode eq 'Y' )
      {
         print "A Call for an issue id was made for $issueId \n";
      }
   }
   return $issueId2Project{$issueId};
}

sub linkIssueId2Project
{
   my ( $projectId, $issueId ) = @_;
   $issueId2Project{$issueId} = $projectId;

   mkdir $path2Export
     . getProjectNameFromId($projectId)
     . $slash4Folder
     . $issuesExFolder
     . $slash4Folder
     . $issueId;
   mkdir $path2Export
     . getProjectNameFromId($projectId)
     . $slash4Folder
     . $issuesExFolder
     . $slash4Folder
     . $issueId
     . $slash4Folder
     . $attachmentExFolder;
   mkdir $path2Export
     . getProjectNameFromId($projectId)
     . $slash4Folder
     . $issuesExFolder
     . $slash4Folder
     . $issueId
     . $slash4Folder
     . $commentExFolder;
   mkdir $path2Export
     . getProjectNameFromId($projectId)
     . $slash4Folder
     . $issuesExFolder
     . $slash4Folder
     . $issueId
     . $slash4Folder
     . $worklogExFolder;

   return;
}

sub saveLinkMap
{
   my $tmpFileHandle;
   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   open( $tmpFileHandle, '>' . $path2Export . 'issues2project.txt' );
   print $tmpFileHandle $json_obj->encode( \%issueId2Project );
   close($tmpFileHandle);
   return;
}

sub loadLinkMap
{
   my $tmpFileHandle;
   my $json_obj = JSON->new->allow_blessed->convert_blessed;

   open( $tmpFileHandle, $path2Export . 'issues2project.txt' );
   select( ( select($tmpFileHandle), $/ = undef )[0] );
   my $contents = <$tmpFileHandle>;

   %issueId2Project = %{ $json_obj->decode($contents) };
   close($tmpFileHandle);
   return;
}

sub getLinkNameFromId
{
   my ($linkIdNeedingName) = @_;
   return $globalIssueLinkType{$linkIdNeedingName}->linkname;
}

sub getUserName
{
   my ($name2MapFrom) = @_;
   my $name2Reutrn = $name2MapFrom;
   if ( defined $name2MapFrom and exists $userNameMaps{$name2MapFrom} )
   {
      $name2Reutrn = $userNameMaps{$name2MapFrom};
   }
   return $name2Reutrn;
}

sub writeJIRAObjectOut
{
   my ( $project4Object, $objectName, $pointer2Object ) = @_;

   my $tmpFileHandle;
   my $thing2Use4Path;
   my $json_obj = JSON->new->allow_blessed->convert_blessed->allow_nonref;
   $json_obj->pretty(1);

   if ( $objectName eq 'Component' )
   {
      open(
            $tmpFileHandle,
            '>'
              . $path2Export
              . getProjectNameFromId($project4Object)
              . $slash4Folder
              . $ComponentsExFolder
              . $slash4Folder
              . $pointer2Object->name
              . $exportFilesSuffix
      );
      print $tmpFileHandle $json_obj->encode($pointer2Object);
      close($tmpFileHandle);
   }
   elsif ( $objectName eq 'Version' )
   {
      open(
            $tmpFileHandle,
            '>'
              . $path2Export
              . getProjectNameFromId($project4Object)
              . $slash4Folder
              . $VersionExFolder
              . $slash4Folder
              . $pointer2Object->name
              . $exportFilesSuffix
      );
      print $tmpFileHandle $json_obj->encode($pointer2Object);
      close($tmpFileHandle);
   }
   elsif ( $objectName eq 'Issue' )
   {
      linkIssueId2Project( $project4Object, $pointer2Object->externalId );
      open(
            $tmpFileHandle,
            '>'
              . $path2Export
              . getProjectNameFromId($project4Object)
              . $slash4Folder
              . $issuesExFolder
              . $slash4Folder
              . $pointer2Object->externalId
              . $slash4Folder
              . $pointer2Object->externalId
              . $exportFilesSuffix
        )
        or die "Unable to open "
        . $path2Export
        . getProjectNameFromId($project4Object)
        . $slash4Folder
        . $issuesExFolder
        . $slash4Folder
        . $pointer2Object->externalId
        . $slash4Folder
        . $pointer2Object->externalId
        . $exportFilesSuffix;
     if ( $DebugMode eq 'Y' )
      {
         print $path2Export
            . getProjectNameFromId($project4Object)
            . $slash4Folder
            . $issuesExFolder
            . $slash4Folder
            . $pointer2Object->externalId
            . $slash4Folder
            . $pointer2Object->externalId
            . $exportFilesSuffix . "\n";
      }
      my $string2Output = $json_obj->encode($pointer2Object);
      if ($string2Output =~ /[^!-~\s]/g)
      {
            if($DebugMode eq 'Y')
            {
               print getProjectNameFromId($project4Object)." for issue has invalid char(s):\n".$string2Output."----END OF Object\n";
            }
           $string2Output = correctBadEncoding($string2Output);         
      }
      print $tmpFileHandle $string2Output ;
      close($tmpFileHandle);
   }
   elsif ( $objectName eq $attachmentExFolder )
   {
      $fileAtchCount = $fileAtchCount + 1;
      if ( foundIssue2ProjectLink($project4Object) )
      {
         $thing2Use4Path =
             $path2Export
           . getProjectNameFromId( getProjectIdFromIssueId($project4Object) )
           . $slash4Folder
           . $issuesExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $attachmentExFolder
           . $slash4Folder
           . $fileAtchCount
           . $exportFilesSuffix;
      }
      else
      {
         $thing2Use4Path =
             $path2Export
           . $unmappedExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $attachmentExFolder
           . $slash4Folder
           . $fileAtchCount
           . $exportFilesSuffix;
      }
      open( $tmpFileHandle, '>' . $thing2Use4Path );
      print $tmpFileHandle $json_obj->encode($pointer2Object);
      close($tmpFileHandle);
   }
   elsif ( $objectName eq $commentExFolder )
   {
      $commentCount = $commentCount + 1;
      if ( foundIssue2ProjectLink($project4Object) )
      {
         $thing2Use4Path =
             $path2Export
           . getProjectNameFromId( getProjectIdFromIssueId($project4Object) )
           . $slash4Folder
           . $issuesExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $commentExFolder
           . $slash4Folder
           . $commentCount
           . $exportFilesSuffix;
      }
      else
      {
         $thing2Use4Path =
             $path2Export
           . $unmappedExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $commentExFolder
           . $slash4Folder
           . $commentCount
           . $exportFilesSuffix;
      }
      open( $tmpFileHandle, '>' . $thing2Use4Path );
      if ( $DebugMode eq 'Y' )
      {
         print $thing2Use4Path."\n";
      }
      my $string2Output = $json_obj->encode($pointer2Object);
      if ($string2Output =~ /[^!-~\s]/g){
           if($DebugMode eq 'Y')
            {
               print "The following comment with project: ".getProjectNameFromId( getProjectIdFromIssueId($project4Object) ).' and ISSUE ID:'.$project4Object." has invalid char(s):\n".$string2Output."----END OF Object\n";   
            }
           $string2Output = correctBadEncoding($string2Output);
      }
      print $tmpFileHandle $string2Output;
      close($tmpFileHandle);
   }
   elsif ( $objectName eq $worklogExFolder )
   {
      $workLogCount = $workLogCount + 1;
      if ( foundIssue2ProjectLink($project4Object) )
      {
         $thing2Use4Path =
             $path2Export
           . getProjectNameFromId( getProjectIdFromIssueId($project4Object) )
           . $slash4Folder
           . $issuesExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $worklogExFolder
           . $slash4Folder
           . $workLogCount
           . $exportFilesSuffix;
      }
      else
      {
         $thing2Use4Path =
             $path2Export
           . $unmappedExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $worklogExFolder
           . $slash4Folder
           . $workLogCount
           . $exportFilesSuffix;
      }
      open( $tmpFileHandle, '>' . $thing2Use4Path ) or die "Unable to open $thing2Use4Path";
      if ( $DebugMode eq 'Y' )
      {
         print $thing2Use4Path. "\n";
      }
      my $string2Output = $json_obj->encode($pointer2Object);
      if ($string2Output =~ /[^!-~\s]/g)
      {
           if($DebugMode eq 'Y')
           {
            print "The following comment with project: ".getProjectNameFromId( getProjectIdFromIssueId($project4Object) ).' and ISSUE ID:'.$project4Object." has invalid char(s):\n".$string2Output."----END OF Object\n";   
           }
           $string2Output = correctBadEncoding($string2Output);
      }
      print $tmpFileHandle $string2Output;
      close($tmpFileHandle);
   }
   elsif ( $objectName eq 'Label' )
   {
      if ( foundIssue2ProjectLink($project4Object) )
      {
         $thing2Use4Path =
             $path2Export
           . getProjectNameFromId( getProjectIdFromIssueId($project4Object) )
           . $slash4Folder
           . $issuesExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $labelsExSuffix
           . $exportFilesSuffix;
      }
      else
      {
         $thing2Use4Path =
             $path2Export
           . $unmappedExFolder
           . $slash4Folder
           . $project4Object
           . $slash4Folder
           . $labelsExSuffix
           . $exportFilesSuffix;
      }
      open( $tmpFileHandle, '>>' . $thing2Use4Path )
        or die "Unable to open $thing2Use4Path";
      print $tmpFileHandle $pointer2Object
        . $sep4Labels;    #$json_obj->encode($pointer2Object);
      close($tmpFileHandle);
   }
   return;
}

sub correctBadEncoding
{
   my ($jsonTextNeedingCode) = (@_);
   my $string2Move = $jsonTextNeedingCode;
   #$string2Move =~ tr/\x20-\x7f//cd; #this removes all the invalid chars
   #TODO:add support for mapping the invalid chars over to asci ones
#   $string2Move =~ s/�/-/g;
#   $string2Move =~ s/�/\"/g;
#   $string2Move =~ s/�/./g;
#   $string2Move =~ s/�/\"/g;
#   $string2Move =~ s/�/../g;
#   $string2Move =~ s/–/-/gu;
#   $string2Move =~ s/’/./gu;
#   $string2Move =~ s/•/./gu;
#   $string2Move =~ s/“/"/gu;
#   $string2Move =~ s/…/.../gu;
#   $string2Move =~ s/�?/\"/gu;      
   return $string2Move;
}

sub writeJiraLinkObjectOut
{
   my ( $linkID, $linkPointer ) = @_;
   my $tmpFileHandle;
   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   open(
         $tmpFileHandle,
         '>'
           . $path2Export
           . $linksExFolder
           . $slash4Folder
           . $linkID
           . $exportFilesSuffix
   );
   print $tmpFileHandle $json_obj->encode($linkPointer);
   close($tmpFileHandle);

   return;
}

sub moveUnmappedFolders
{
#its assumed that %issueId2Project is filled and that getProjectNameFromId will return the project key if there is an id
   opendir( my $dh, $path2Export . $unmappedExFolder . $slash4Folder ) || die;
   while ( my $issueId2Move = readdir($dh) )
   {
      if ( isThereAProjectLink($issueId2Move) )
      {
         my $tmpFileHandle;

         #moving labels
         if (   -e $path2Export
              . $unmappedExFolder
              . $slash4Folder
              . $issueId2Move
              . $slash4Folder
              . $labelsExSuffix
              . $exportFilesSuffix )
         {

            open(
                  $tmpFileHandle,
                  $path2Export
                    . $unmappedExFolder
                    . $slash4Folder
                    . $issueId2Move
                    . $slash4Folder
                    . $labelsExSuffix
                    . $exportFilesSuffix
            );
            select( ( select($tmpFileHandle), $/ = undef )[0] );
            my $contents = <$tmpFileHandle>;

            open(
                  $tmpFileHandle,
                  '>>'
                    . $path2Export
                    . getProjectNameFromId(
                                          getProjectIdFromIssueId($issueId2Move)
                    )
                    . $slash4Folder
                    . $issuesExFolder
                    . $slash4Folder
                    . $issueId2Move
                    . $slash4Folder
                    . $labelsExSuffix
                    . $exportFilesSuffix
            );
            print $tmpFileHandle $contents;
            close($tmpFileHandle);
         }

         #moving comments
         if (   -e $path2Export
              . $unmappedExFolder
              . $slash4Folder
              . $issueId2Move
              . $slash4Folder
              . $commentExFolder
              . $slash4Folder )
         {
            moveAllFilesFromOneDir2Other(
                                     $path2Export
                                       . $unmappedExFolder
                                       . $slash4Folder
                                       . $issueId2Move
                                       . $slash4Folder
                                       . $commentExFolder
                                       . $slash4Folder,
                                     $path2Export
                                       . getProjectNameFromId(
                                        getProjectIdFromIssueId($issueId2Move) )
                                       . $slash4Folder
                                       . $issuesExFolder
                                       . $slash4Folder
                                       . $issueId2Move
                                       . $slash4Folder
                                       . $commentExFolder
                                       . $slash4Folder
            );
         }

         #moving attachments
         if (   -e $path2Export
              . $unmappedExFolder
              . $slash4Folder
              . $issueId2Move
              . $slash4Folder
              . $attachmentExFolder
              . $slash4Folder )
         {
            moveAllFilesFromOneDir2Other(
                                     $path2Export
                                       . $unmappedExFolder
                                       . $slash4Folder
                                       . $issueId2Move
                                       . $slash4Folder
                                       . $attachmentExFolder
                                       . $slash4Folder,
                                     $path2Export
                                       . getProjectNameFromId(
                                        getProjectIdFromIssueId($issueId2Move) )
                                       . $slash4Folder
                                       . $issuesExFolder
                                       . $slash4Folder
                                       . $issueId2Move
                                       . $slash4Folder
                                       . $attachmentExFolder
                                       . $slash4Folder
            );
         }

         #moving worklogs
         if (   -e $path2Export
              . $unmappedExFolder
              . $slash4Folder
              . $issueId2Move
              . $slash4Folder
              . $worklogExFolder
              . $slash4Folder )
         {
            moveAllFilesFromOneDir2Other(
                                     $path2Export
                                       . $unmappedExFolder
                                       . $slash4Folder
                                       . $issueId2Move
                                       . $slash4Folder
                                       . $worklogExFolder
                                       . $slash4Folder,
                                     $path2Export
                                       . getProjectNameFromId(
                                        getProjectIdFromIssueId($issueId2Move) )
                                       . $slash4Folder
                                       . $issuesExFolder
                                       . $slash4Folder
                                       . $issueId2Move
                                       . $slash4Folder
                                       . $worklogExFolder
                                       . $slash4Folder
            );
         }
         rmtree $path2Export. $unmappedExFolder . $slash4Folder . $issueId2Move;
      }
   }
   close($dh);
   return;
}

sub moveAllFilesFromOneDir2Other
{
   my ( $sourceDir, $dir2MoveThings ) = @_;
   opendir( my $dh2Handle, $sourceDir ) || die;
   while ( my $file2Move = readdir($dh2Handle) )
   {
      move( $sourceDir . $file2Move, $dir2MoveThings . $file2Move );
   }
   close($dh2Handle);
   return;
}

sub getSubTaskLinksAndFinalLinks
{
   #note:sub tasks are used as as issue links
   #<IssueLink id="10903" linktype="10100" source="11210" destination="11608" sequence="4"/> #destination is parent
   my $linkHolder   = JIRAImportObjects::GlobalHolder->new;
   my @links2Export = ();
   
   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);
   
   opendir( my $dh2Handle, $path2Export . $linksExFolder . $slash4Folder );
   while ( my $file2Move = readdir($dh2Handle) )
   {
      if( not ($file2Move eq '.' or $file2Move eq '..'))
      {
         open( my $tmpFileHandle, $path2Export . $linksExFolder . $slash4Folder.$file2Move )  or die "unable to read $file2Move";
         select( ( select($tmpFileHandle), $/ = undef )[0] );
         my $contents = <$tmpFileHandle>;
         close($tmpFileHandle);
      
         my $item2Return =  JIRAImportObjects::Link->new( %{ $json_obj->decode($contents) } );
      
         my $parrentID = $item2Return->destinationId;
         if($item2Return->name eq $subTaskLinkType)
         {
            if(exists $subTasks{$parrentID})
            {
               push (@{$subTasks{$parrentID}},$item2Return->sourceId )
            }else
            {
               my @array4Ids = ();
               push (@array4Ids,$item2Return->sourceId);
               $subTasks{$parrentID} = \@array4Ids;
            }
         }else{
            push (@links2Export,$item2Return);
         }
      }
   }
   close($dh2Handle);
   $linkHolder->links( \@links2Export );
   
   open(
         my $tmpFileHandle,
         '>'
           . $path2Export
           . 'Complete_Export'
           . $slash4Folder
           . $linksExFolder
           . $exportFilesSuffix
   );
   print $tmpFileHandle $json_obj->encode($linkHolder);
   close($tmpFileHandle);
   
   return $linkHolder;
}

sub buildOutputFileForProject
{
   my ($projectKey2WorkOn) = @_;
   my $projectHolder       = JIRAImportObjects::GlobalHolder->new;
   my $finalProject4Export = JIRAImportObjects::Project->new;
   my $json_obj            = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   $finalProject4Export->key($projectKey2WorkOn);
   my $globalJsonObject = getJiraGlobalObjectFromKey($projectKey2WorkOn);
   if ( !defined $globalJsonObject )
   {
      die 'Invalid Project for ' . $projectKey2WorkOn;
   }
   $finalProject4Export->name( $globalJsonObject->name );
   $finalProject4Export->avatarUrl(
      getProjectAvatarURL( $globalJsonObject->avatar, $globalJsonObject->id ) );
   $finalProject4Export->description( $globalJsonObject->description );
   $finalProject4Export->workflowSchemeName( getWorkFlow($projectKey2WorkOn) );
   my @components4Projects = returnArrayOfComponents($projectKey2WorkOn);
   $finalProject4Export->components( \@components4Projects );
   my @versions4Projects = returnArrayOfVersions($projectKey2WorkOn);
   $finalProject4Export->versions( \@versions4Projects );
   my @issues4Project = returnArrayOfIssues( $projectKey2WorkOn, 0 );
   $finalProject4Export->issues( \@issues4Project );
   
   my @singleProject = ($finalProject4Export);
   $projectHolder->projects( \@singleProject );
   my @projectSuBTasksLinks = fetchSubIssueLinksArray($projectKey2WorkOn);
   $projectHolder->links(\@projectSuBTasksLinks);
   
   open(
         my $tmpFileHandle,
         '>'
           . $path2Export
           . 'Complete_Export'
           . $slash4Folder
           . $projectKey2WorkOn
           . $exportFilesSuffix
   );
   print $tmpFileHandle $json_obj->encode($projectHolder);
   close($tmpFileHandle);
   
   return $projectHolder;
}

sub returnArrayOfComponents
{
   my ($projectKey2WorkOn) = @_;
   my @arrayOfComps;
   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   opendir( my $dh2Handle,
         $path2Export . $projectKey2WorkOn . $slash4Folder . $ComponentsExFolder );
   while ( my $file2Move = readdir($dh2Handle) )
   {
      if ( !( $file2Move eq '.' ) and !( $file2Move eq '..' ) )
      {
         open(
               my $tmpFileHandle,
               $path2Export
                 . $projectKey2WorkOn
                 . $slash4Folder
                 . $ComponentsExFolder
                 . $slash4Folder
                 . $file2Move
           )
           or die 'couldnt open file'
           . $path2Export
           . $projectKey2WorkOn
           . $slash4Folder
           . $ComponentsExFolder
           . $file2Move;
         select( ( select($tmpFileHandle), $/ = undef )[0] );
         my $contents = <$tmpFileHandle>;
         close($tmpFileHandle);
         my $comp2Move = JIRAImportObjects::Component->new(
                                            %{ $json_obj->decode($contents) } );
         push( @arrayOfComps, $comp2Move );
      }
   }
   close($dh2Handle);

   return @arrayOfComps;
}

sub returnArrayOfVersions
{
   my ($projectKey2WorkOn) = @_;
   my @arrayOfVers;
   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   opendir( my $dh2Handle,
      $path2Export . $projectKey2WorkOn . $slash4Folder . $VersionExFolder );
   while ( my $file2Move = readdir($dh2Handle) )
   {
      if ( !( $file2Move eq '.' ) and !( $file2Move eq '..' ) )
      {
         open(
               my $tmpFileHandle,
               $path2Export
                 . $projectKey2WorkOn
                 . $slash4Folder
                 . $VersionExFolder
                 . $slash4Folder
                 . $file2Move
         );
         select( ( select($tmpFileHandle), $/ = undef )[0] );
         my $contents = <$tmpFileHandle>;
         close($tmpFileHandle);
         my $ver2Move =
           JIRAImportObjects::Version->new( %{ $json_obj->decode($contents) } );
         push( @arrayOfVers, $ver2Move );
      }
   }
   close($dh2Handle);

   return @arrayOfVers;
}

sub returnArrayOfIssues
{
   my ($projectKey2WorkOn) = @_;
   my @arrayOfIssues;
   opendir(
            my $dh2Handle,
            $path2Export
              . $projectKey2WorkOn
              . $slash4Folder
              . $issuesExFolder
              . $slash4Folder
   );
   while ( my $file2Move = readdir($dh2Handle) )
   {
      if (   -d $path2Export
           . $projectKey2WorkOn
           . $slash4Folder
           . $issuesExFolder
           . $slash4Folder
           . $file2Move and not( $file2Move eq '..' or $file2Move eq '.' ) )
      {
         my $issu2Load = fetchSingleIssue( $projectKey2WorkOn, $file2Move, 1 );
         push( @arrayOfIssues, $issu2Load );
      }

   }
   close($dh2Handle);

#$path2Export.getProjectNameFromId($project4Object).$slash4Folder.$issuesExFolder.$slash4Folder.$pointer2Object->externalId.$slash4Folder.$pointer2Object->externalId.$exportFilesSuffix
   return @arrayOfIssues;
}

sub fetchSingleIssue
{
   my ( $projectKey2WorkOn, $file2Move, $fetchSubTasks ) = @_;
   my $issu2Load =
     loadIssueFromFile(   $path2Export
                        . $projectKey2WorkOn
                        . $slash4Folder
                        . $issuesExFolder
                        . $slash4Folder
                        . $file2Move
                        . $slash4Folder
                        . $file2Move
                        . $exportFilesSuffix );

   #comments #
   my @comments2Add;
   opendir(
            my $dh2HandleC,
            $path2Export
              . $projectKey2WorkOn
              . $slash4Folder
              . $issuesExFolder
              . $slash4Folder
              . $file2Move
              . $slash4Folder
              . $commentExFolder
              . $slash4Folder
   );
   while ( my $subItem2Map = readdir($dh2HandleC) )
   {
      if ( not( $subItem2Map eq '..' or $subItem2Map eq '.' ) )
      {
         my $singleItm2Map =
           loadsubItemFromFile(
                                'comments',
                                $path2Export
                                  . $projectKey2WorkOn
                                  . $slash4Folder
                                  . $issuesExFolder
                                  . $slash4Folder
                                  . $file2Move
                                  . $slash4Folder
                                  . $commentExFolder
                                  . $slash4Folder
                                  . $subItem2Map
           );
         push( @comments2Add, $singleItm2Map );
      }
   }
   close($dh2HandleC);
   $issu2Load->comments( \@comments2Add );

   #attachments#
   my @atch2Add;
   opendir(
            my $dh2HandleA,
            $path2Export
              . $projectKey2WorkOn
              . $slash4Folder
              . $issuesExFolder
              . $slash4Folder
              . $file2Move
              . $slash4Folder
              . $attachmentExFolder
              . $slash4Folder
   );
   while ( my $subItem2Map = readdir($dh2HandleA) )
   {
      if ( not( $subItem2Map eq '..' or $subItem2Map eq '.' ) )
      {
         my $singleItm2Map =
           loadsubItemFromFile(
                                'attachments',
                                $path2Export
                                  . $projectKey2WorkOn
                                  . $slash4Folder
                                  . $issuesExFolder
                                  . $slash4Folder
                                  . $file2Move
                                  . $slash4Folder
                                  . $attachmentExFolder
                                  . $slash4Folder
                                  . $subItem2Map
           );
         push( @atch2Add, $singleItm2Map );
      }
   }
   close($dh2HandleA);
   $issu2Load->attachments( \@atch2Add );

   #worklogs #
   my @work2Add;
   opendir(
            my $dh2HandleW,
            $path2Export
              . $projectKey2WorkOn
              . $slash4Folder
              . $issuesExFolder
              . $slash4Folder
              . $file2Move
              . $slash4Folder
              . $worklogExFolder
              . $slash4Folder
   );
   while ( my $subItem2Map = readdir($dh2HandleW) )
   {
      if ( not( $subItem2Map eq '..' or $subItem2Map eq '.' ) )
      {
         my $singleItm2Map =
           loadsubItemFromFile(
                                'worklogs',
                                $path2Export
                                  . $projectKey2WorkOn
                                  . $slash4Folder
                                  . $issuesExFolder
                                  . $slash4Folder
                                  . $file2Move
                                  . $slash4Folder
                                  . $worklogExFolder
                                  . $slash4Folder
                                  . $subItem2Map
           );
         push( @work2Add, $singleItm2Map );
      }
   }
   close($dh2HandleW);
   $issu2Load->worklogs( \@work2Add );
   my @labels2Match = loadLabelsFromFile(
                                                $path2Export
                                              . $projectKey2WorkOn
                                              . $slash4Folder
                                              . $issuesExFolder
                                              . $slash4Folder
                                              . $file2Move
                                              . $slash4Folder
                                              . $labelsExSuffix
                                              . $exportFilesSuffix
                       );
   $issu2Load->labels(\@labels2Match);
   
#TODO: I don't think i need to do subtasks this way
   #subtasks
#   if ($fetchSubTasks)
#   {
#      my @subTasks4Issue = getSubTasks( $projectKey2WorkOn, $file2Move );
#      $issu2Load->subtasks( \@subTasks4Issue );
#   }

   #TODO:watchers #not supported at this time
   #TODO:affectedVersions#not supported at this time
   #TODO:CustomField#not supported at this time

   return $issu2Load;
}

=for getSubTasks
#sub getSubTasks
#{
#   my ( $projectKey2WorkOn, $issueNeedingSubTasks ) = @_;
#   my @subTasks2Return;
#   if ( exists $subTasks{$issueNeedingSubTasks} )
#   {
#      my @arrayOfSubTasks = @{ $subTasks{$issueNeedingSubTasks} };
#      foreach my $id4SubTask (@arrayOfSubTasks)
#      {
#         push( @subTasks2Return,
#               fetchSingleIssue( $projectKey2WorkOn, $id4SubTask, 0 ) );
#      }
#   }
#
#   return @subTasks2Return;
#}
=cut

sub fetchSubIssueLinksArray
{
   my ($parrentProject ) = @_;
   my @arrayOfLinks = ();
   foreach my $link2Consider( keys (%subTasks))
   {
      if(exists($issueId2Project{$link2Consider}) and getProjectNameFromId($issueId2Project{$link2Consider}) eq $parrentProject)
      {
         my @arrayOfTasks = @{$subTasks{$link2Consider}};
         foreach my $subTaskId (@arrayOfTasks)
         {
            my $task4Set =  JIRAImportObjects::Link->new;
            $task4Set->name($subTaskLinkType);
            $task4Set->sourceId($subTaskId);
            $task4Set->destinationId($link2Consider);
            push(@arrayOfLinks, $task4Set);
         }
      }
   }
   
   #todo:add code to use it
   return @arrayOfLinks;
}

sub loadIssueFromFile
{
   my ($fileHoldingIssue) = @_;
   my $issueItemLoaded;

   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   open( my $tmpFileHandle, $fileHoldingIssue );
   select( ( select($tmpFileHandle), $/ = undef )[0] );
   my $contents = <$tmpFileHandle>;
   close($tmpFileHandle);

   $issueItemLoaded =
     JIRAImportObjects::Issue->new( %{ $json_obj->decode($contents) } );

   return $issueItemLoaded;
}

sub loadsubItemFromFile
{
   my ( $itemType, $fileHoldingItem ) = @_;
   my $item2Return;

   my $json_obj = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);

   open( my $tmpFileHandle, $fileHoldingItem )
     or die "unable to read $fileHoldingItem for $itemType";
   select( ( select($tmpFileHandle), $/ = undef )[0] );
   my $contents = <$tmpFileHandle>;
   close($tmpFileHandle);

   if ( $itemType eq 'comments' )
   {
      $item2Return =
        JIRAImportObjects::Comment->new( %{ $json_obj->decode($contents) } );
   }
   elsif ( $itemType eq 'worklogs' )
   {
      $item2Return =
        JIRAImportObjects::WorkLog->new( %{ $json_obj->decode($contents) } );
   }
   elsif ( $itemType eq 'attachments' )
   {
      $item2Return =
        JIRAImportObjects::Attachment->new( %{ $json_obj->decode($contents) } );
   }

   return $item2Return;

}

sub loadLabelsFromFile
{
   my ($fileHoldingItem) = @_;
   my @array4Labels = ();
   if (-e $fileHoldingItem) 
   {
      open( my $tmpFileHandle, $fileHoldingItem )    or die "unable to read $fileHoldingItem";
      my $singleLine = <$tmpFileHandle>;
      @array4Labels = split($sep4Labels, $singleLine);
      close($tmpFileHandle);
   }
   if(defined $systemImportLabel)
   {
      push( @array4Labels, $systemImportLabel );
   }
   return @array4Labels;
}

sub getJiraGlobalObjectFromKey
{
   my ($key2Use) = @_;
   my $globalProject2Find;
   foreach my $key ( keys %globalProject )
   {
      if ( $globalProject{$key}->key eq $key2Use )
      {
         $globalProject2Find = $globalProject{$key};
      }
   }
   return $globalProject2Find;
}

sub buildFinalOutPutProjectFiles
{
   mkdir $path2Export . 'Complete_Export';
   my $fullProjectList       = JIRAImportObjects::GlobalHolder->new;
   my $finalProject4Export = JIRAImportObjects::Project->new;
   my $json_obj            = JSON->new->allow_blessed->convert_blessed;
   $json_obj->pretty(1);
   
   $fullProjectList = getSubTaskLinksAndFinalLinks();
   my @emptyArray = ();
   $fullProjectList->projects(\@emptyArray);
   opendir( my $dh, $path2Export . $slash4Folder ) || die;
   while ( my $project2Export = readdir($dh) )
   {
      if (     !( $project2Export eq $linksExFolder )
           and !( $project2Export eq $unmappedExFolder )
           and !( $project2Export eq 'Complete_Export' )
           and !( $project2Export eq '.' )
           and !( $project2Export eq '..' ) )
      {
         if ( -d $path2Export . $slash4Folder . $project2Export )
         {
           my $projectHolder =  buildOutputFileForProject($project2Export);
           push @{$fullProjectList->projects}, @{$projectHolder->projects};
           push @{$fullProjectList->links}, @{$projectHolder->links};
         }
      }
   }
   
    open(
         my $tmpFileHandle,
         '>'
           . $path2Export
           . 'Complete_Export'
           . $slash4Folder
           . $fileName4FullExport
           .$exportFilesSuffix
   );
   print $tmpFileHandle $json_obj->encode($fullProjectList);
   close($tmpFileHandle);
   
   
}

sub getProjectAvatarURL
{
   my ( $avID, $pjid ) = @_;
   return $avatarUrl . 'pid=' . $pjid . '&avatarId=' . $avID;
}

sub getWorkFlow
{
   my ($projectKey) = @_;
   my $val2Return = $defaultWorkflow;
   if ( exists $projectKey2WorkFlowMap{$projectKey} )
   {
      $val2Return = $projectKey2WorkFlowMap{$projectKey};
   }
   return $val2Return;
}
