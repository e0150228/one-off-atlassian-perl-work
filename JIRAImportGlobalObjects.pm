#!/usr/bin/env perl
=for Licensee
#####BSD License Definition#####
#    Copyright 2012 Windstream. All rights reserved.
#    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#    THIS SOFTWARE IS PROVIDED BY Windstream "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL WINDSTREAM OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#    The views and conclusions contained in the software and documentation are those of the authors and should not be interpreted as representing official policies, either expressed or implied, of Windstream.
################################
=cut
=for Metadata
#Created:Aug 29, 2013
#Author:Michael Bostwick
#Version:1.00
#Purpose:Modules for Global Jira Objects
=cut
package JIRAImportGlobalObjects::Project;
use Moose; 
   has 'id' => (is => 'rw', isa => 'Maybe[Str]');
   has 'key' => (is => 'rw', isa => 'Maybe[Str]');
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
   has 'avatar' => (is => 'rw', isa => 'Maybe[Str]');
1;
package JIRAImportGlobalObjects::Priority;
use Moose; 
   has 'id' => (is => 'rw', isa => 'Maybe[Str]');
   has 'sequence' => (is => 'rw', isa => 'Maybe[Str]');
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
1;
package JIRAImportGlobalObjects::Status;
use Moose; 
   has 'id' => (is => 'rw', isa => 'Maybe[Str]');
   has 'sequence' => (is => 'rw', isa => 'Maybe[Str]');
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
   has 'iconurl' => (is => 'rw', isa => 'Maybe[Str]');
1;
package JIRAImportGlobalObjects::Resolution;
use Moose; 
   has 'id' => (is => 'rw', isa => 'Maybe[Str]');
   has 'sequence' => (is => 'rw', isa => 'Maybe[Str]');
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
1;
package JIRAImportGlobalObjects::IssueType;
use Moose; 
   has 'id' => (is => 'rw', isa => 'Maybe[Str]');
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'sequence' => (is => 'rw', isa => 'Maybe[Str]');
   has 'style' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
   has 'iconurl' => (is => 'rw', isa => 'Maybe[Str]');
1;
package JIRAImportGlobalObjects::IssueLinkType;
use Moose; 
   has 'id' => (is => 'rw', isa => 'Maybe[Str]');
   has 'linkname' => (is => 'rw', isa => 'Maybe[Str]');
   has 'inward' => (is => 'rw', isa => 'Maybe[Str]');
   has 'outward' => (is => 'rw', isa => 'Maybe[Str]');
   has 'style' => (is => 'rw', isa => 'Maybe[Str]');
1;