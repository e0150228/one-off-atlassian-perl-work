#!/usr/bin/env perl
=for Licensee
#####BSD License Definition#####
#    Copyright 2012 Windstream. All rights reserved.
#    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#    THIS SOFTWARE IS PROVIDED BY Windstream "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL WINDSTREAM OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#    The views and conclusions contained in the software and documentation are those of the authors and should not be interpreted as representing official policies, either expressed or implied, of Windstream.
################################
=cut
=for Metadata
#Created:8/27/2013
#Author:Michael Bostwick
#Version:1.00
#Purpose:Import Objects, which allows perl manpulation including JSON Export.
=cut

package JIRAImportObjects::Attachment;
use Moose;

   has 'attacher' => (is => 'rw', isa => 'Maybe[Str]');
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'uri' => (is => 'rw', isa => 'Maybe[Str]');
   has 'created' => (is => 'rw', isa => 'Maybe[Str]');
 
1;


package JIRAImportObjects::Comment;
use Moose;
   has 'body' => (is => 'rw', isa => 'Maybe[Str]');
   has 'author' => (is => 'rw', isa => 'Maybe[Str]');
   has 'created' => (is => 'rw', isa => 'Maybe[Str]');
 
1;


package JIRAImportObjects::WorkLog;
use Moose;
   has 'author' => (is => 'rw', isa => 'Maybe[Str]');
   has 'comment' => (is => 'rw', isa => 'Maybe[Str]');
   has 'startDate' => (is => 'rw', isa => 'Maybe[Str]');
   has 'timeSpent' => (is => 'rw', isa => 'Maybe[Str]');
1;


package JIRAImportObjects::Component;
use Moose;

   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'lead' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
 
1;


package JIRAImportObjects::Version;
use Moose;
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
   has 'archived' => (is => 'rw');#isa => 'Maybe[Int]'
   has 'released' => (is => 'rw');#isa => 'Maybe[Int]'
1;


package JIRAImportObjects::CustomField;
use Moose;
   has 'fieldName' => (is => 'rw', isa => 'Maybe[Str]');
   has 'fieldType' => (is => 'rw', isa => 'Maybe[Str]');
   has 'value' => (is => 'rw');
1;


package JIRAImportObjects::Link;
use Moose;
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'sourceId' => (is => 'rw', isa => 'Maybe[Str]');
   has 'destinationId' => (is => 'rw', isa => 'Maybe[Str]');
1;
package JIRAImportObjects::RemoteLink;
use Moose; 
   has 'issueid' => (is => 'rw', isa => 'Maybe[Str]');
   has 'title' => (is => 'rw', isa => 'Maybe[Str]');
   has 'url' => (is => 'rw', isa => 'Maybe[Str]');
   has 'relationship' => (is => 'rw', isa => 'Maybe[Str]');
   has 'applicationtype' => (is => 'rw', isa => 'Maybe[Str]');
   has 'applicationname' => (is => 'rw', isa => 'Maybe[Str]');
1;

package JIRAImportObjects::Issue;
use Moose; 
   has 'externalId' => (is => 'rw', isa => 'Maybe[Str]');
   has 'status' => (is => 'rw', isa => 'Maybe[Str]');
   has 'reporter' => (is => 'rw', isa => 'Maybe[Str]');
   has 'assignee' => (is => 'rw', isa => 'Maybe[Str]');
   has 'created' => (is => 'rw', isa => 'Maybe[Str]');
   has 'updated' => (is => 'rw', isa => 'Maybe[Str]');
   has 'duedate' => (is => 'rw', isa => 'Maybe[Str]');
   has 'summary' => (is => 'rw', isa => 'Maybe[Str]');
   has 'issueType' => (is => 'rw', isa => 'Maybe[Str]');
   has 'originalEstimate' => (is => 'rw', isa => 'Maybe[Str]');
   has 'estimate' => (is => 'rw', isa => 'Maybe[Str]');
   has 'priority' => (is => 'rw', isa => 'Maybe[Str]');
   has 'resolution' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
   has 'environment' => (is => 'rw', isa => 'Maybe[Str]');
   
   has 'labels' => (is => 'rw',isa => 'ArrayRef');
   has 'watchers' => (is => 'rw');
   has 'affectedVersions' => (is => 'rw');
   
   has 'customFieldValues' => (is => 'rw');
   has 'comments' => (is => 'rw',isa => 'ArrayRef');
   has 'worklogs' => (is => 'rw',isa => 'ArrayRef');
   has 'subtasks' => (is => 'rw',isa => 'ArrayRef');
   has 'attachments' => (is => 'rw',isa => 'ArrayRef');
 
1;

package JIRAImportObjects::Project;
use Moose;
   has 'name' => (is => 'rw', isa => 'Maybe[Str]');
   has 'key' => (is => 'rw', isa => 'Maybe[Str]');
   has 'avatarUrl' => (is => 'rw', isa => 'Maybe[Str]');
   has 'description' => (is => 'rw', isa => 'Maybe[Str]');
   has 'workflowSchemeName' => (is => 'rw', isa => 'Maybe[Str]');
   
   has 'components' => (is => 'rw',isa => 'ArrayRef');
   has 'versions' => (is => 'rw',isa => 'ArrayRef');
   
   has 'issues' => (is => 'rw',isa => 'ArrayRef');
   
   
1;
package JIRAImportObjects::GlobalHolder;
use Moose;

   has 'projects' => (is => 'rw',isa => 'ArrayRef');
   
   has 'links' => (is => 'rw',isa => 'ArrayRef');
1;