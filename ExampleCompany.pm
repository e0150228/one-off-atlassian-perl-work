#information that should not be shared, but may be needed for scripts
package ExampleCompany;

use strict; 
use warnings;

use base 'Exporter';

use Readonly;

our @EXPORT = qw();
our @EXPORT_OK = qw( $JiraURL );

Readonly::Scalar our $JiraURL => 'http://ExampleCompany.atlassian.net/';

1;